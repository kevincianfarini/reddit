package com.kevincianfarini.reddit.data.network

class HttpStatusCodeException(
    private val status: Int,
    private val body: String? = null,
    message: String? = null
) : Exception(message)