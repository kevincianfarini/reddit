package com.kevincianfarini.reddit.data.model

enum class RedditPostTimeFilterKey(val value: String) {
    HOUR("hour"),
    DAY("day"),
    WEEK("week"),
    YEAR("year"),
    ALL_TIME("all")
}