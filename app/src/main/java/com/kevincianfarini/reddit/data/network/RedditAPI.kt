package com.kevincianfarini.reddit.data.network

import com.kevincianfarini.reddit.data.model.PaginatedResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface RedditAPI {

    @GET("r/{subReddit}/{sortKey}.json")
    suspend fun getSubredditListing(
        @Path("subReddit") subReddit: String,
        @Path("sortKey") sortKey: String,
        @Query("after") after: String? = null,
        @Query("before") before: String? = null,
        @Query("t") time: String? = null
    ): Response<PaginatedResponse>
}