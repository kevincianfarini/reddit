package com.kevincianfarini.reddit.data.model

enum class RedditPostSortKey(val descriptor: String, val time: RedditPostTimeFilterKey? = null) {
    BEST("best"),
    HOT("hot"),
    NEW("new"),
    RISING("rising"),
    TOP_HOUR("top", RedditPostTimeFilterKey.HOUR),
    TOP_DAY("top", RedditPostTimeFilterKey.DAY),
    TOP_WEEK("top", RedditPostTimeFilterKey.WEEK),
    TOP_YEAR("top", RedditPostTimeFilterKey.YEAR),
    TOP_ALL_TIME("top", RedditPostTimeFilterKey.ALL_TIME),
    CONTROVERSIAL_HOUR("controversial", RedditPostTimeFilterKey.HOUR),
    CONTROVERSIAL_DAY("controversial", RedditPostTimeFilterKey.DAY),
    CONTROVERSIAL_WEEK("controversial", RedditPostTimeFilterKey.WEEK),
    CONTROVERIAL_YEAR("controversial", RedditPostTimeFilterKey.YEAR),
    CONTROVERSIAL_ALL_TIME("controversial", RedditPostTimeFilterKey.ALL_TIME)
}