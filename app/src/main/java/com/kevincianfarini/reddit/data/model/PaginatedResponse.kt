package com.kevincianfarini.reddit.data.model

import kotlinx.serialization.Serializable

@Serializable
data class PaginatedResponse(private val data: Data) {
    val after = data.after
    val before = data.before
    val children = data.children
}

@Serializable
data class Data(
    val after: String?,
    val before: String?,
    val children: List<Post>
)