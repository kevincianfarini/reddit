package com.kevincianfarini.reddit.data.network

import com.kevincianfarini.reddit.data.model.PaginatedResponse
import com.kevincianfarini.reddit.data.model.RedditPostSortKey
import com.kevincianfarini.reddit.data.model.RedditPostTimeFilterKey
import com.kevincianfarini.reddit.util.getBodyOrThrow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class RedditAPIManager @Inject constructor(private val api: RedditAPI) {

    suspend fun getSubredditPosts(
        subReddit: String,
        sortKey: RedditPostSortKey,
        after: String? = null,
        before: String? = null
    ): PaginatedResponse {
        return api.getSubredditListing(subReddit, sortKey.descriptor, after, before, sortKey.time?.value).getBodyOrThrow()
    }
}