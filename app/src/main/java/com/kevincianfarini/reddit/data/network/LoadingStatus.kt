package com.kevincianfarini.reddit.data.network

sealed class LoadingStatus {
    object Completed : LoadingStatus()
    data class Loading(val type: LoadingType) : LoadingStatus()
    data class Error(val e: Exception) : LoadingStatus()
}

enum class LoadingType {
    REFRESH, PAGINATED
}