package com.kevincianfarini.reddit.data.model

import com.kevincianfarini.reddit.feature.base.adapter.ViewType
import kotlinx.serialization.*
import kotlinx.serialization.internal.LongDescriptor

@Serializable
data class Post(private val data: PostData) : ViewType {
    val author get() = data.author
    val url get() = data.url
    val permalink get() = data.permalink
    val created get() = data.created
    val subreddit get() = data.subreddit
    val text get() = data.subreddit
    val title get() = data.title
    val score get() = data.score
    val thumbnail get() = data.thumbnail
    override val viewType: ViewType.PostType
        get()  = when {
            URL_REGEX.matches(url) -> ViewType.PostType.TEXT
            IMAGE_REGEX.matches(url) -> ViewType.PostType.IMAGE
            else -> ViewType.PostType.LINK
        }

    companion object {
        private val URL_REGEX = Regex("www.https://reddit.com/r/.*/comments/.*")
        private val IMAGE_REGEX = Regex(".*(.jpg|.jpeg|.png|.gif)")
    }
}

@Serializable
data class PostData(
    val author: String,
    val url: String,
    val permalink: String,
    @Serializable(with = LongSerializer::class) val created: Long,
    val subreddit: String,
    val text: String? = null,
    val title: String,
    val score: Int,
    val thumbnail: String
)

internal object LongSerializer : KSerializer<Long> {
    override val descriptor: SerialDescriptor
        get() = LongDescriptor.withName("created")

    override fun deserialize(decoder: Decoder): Long {
        val parsed = decoder.decodeString()
        return parsed.split(".").first().toLong()
    }

    override fun serialize(encoder: Encoder, obj: Long) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}