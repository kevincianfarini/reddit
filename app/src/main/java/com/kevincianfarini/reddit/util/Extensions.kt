package com.kevincianfarini.reddit.util

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kevincianfarini.reddit.data.network.HttpStatusCodeException
import retrofit2.Response

fun <T> Response<T>.getBodyOrThrow(): T {
    if (this.isSuccessful) {
        return this.body()!!
    }
    throw HttpStatusCodeException(this.code(), this.errorBody()?.string(), this.message())
}

fun ViewGroup.inflate(layoutId: Int, attachToRoot: Boolean = false): View = LayoutInflater.from(context).inflate(layoutId, this, attachToRoot)