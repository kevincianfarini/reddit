package com.kevincianfarini.reddit.di

import android.content.Context
import com.kevincianfarini.reddit.RedditApplication
import dagger.Module
import dagger.Provides

@Module
class AppModule(private val app: RedditApplication) {

    @Provides
    fun providesApp() = app

    @Provides
    fun providesContext() = app as Context
}