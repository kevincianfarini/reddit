package com.kevincianfarini.reddit.di

import com.kevincianfarini.reddit.feature.postlist.view.PostListFragment
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class,
        NetworkModule::class
    ]
)
interface AppComponent {

    fun inject(fragment: PostListFragment)
}