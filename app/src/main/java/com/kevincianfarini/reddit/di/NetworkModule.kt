package com.kevincianfarini.reddit.di

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import com.kevincianfarini.reddit.data.network.RedditAPI
import dagger.Module
import dagger.Provides
import kotlinx.serialization.UnstableDefault
import kotlinx.serialization.json.Json
import okhttp3.MediaType
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class NetworkModule {

    @Singleton @Provides @UnstableDefault
    fun providesRetrofit(): RedditAPI = Retrofit.Builder()
        .baseUrl("https://reddit.com/")
        .addConverterFactory(Json.nonstrict.asConverterFactory(MediaType.get("application/json")))
        .build().create(RedditAPI::class.java)
}