package com.kevincianfarini.reddit

import android.app.Application
import com.kevincianfarini.reddit.di.AppComponent
import com.kevincianfarini.reddit.di.AppModule
import com.kevincianfarini.reddit.di.DaggerAppComponent

class RedditApplication : Application() {

    val appComponent: AppComponent by lazy {
        DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .build()
    }
}