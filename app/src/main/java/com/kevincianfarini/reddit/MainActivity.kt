package com.kevincianfarini.reddit

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import com.kevincianfarini.reddit.data.network.RedditAPI
import com.kevincianfarini.reddit.feature.postlist.view.PostListFragment
import kotlinx.coroutines.*
import kotlinx.serialization.UnstableDefault
import kotlinx.serialization.json.Json
import okhttp3.MediaType
import retrofit2.Retrofit

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setFragment(PostListFragment())
    }

    fun setFragment(fragment: Fragment, addToBackStack: Boolean = false) {
        with(supportFragmentManager.beginTransaction()) {
            this.replace(R.id.content, fragment)
            if (addToBackStack) {
                this.addToBackStack(null)
            }
            this.commit()
        }
    }
}
