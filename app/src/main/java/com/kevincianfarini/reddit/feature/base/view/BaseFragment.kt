package com.kevincianfarini.reddit.feature.base.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.CallSuper
import androidx.fragment.app.Fragment
import com.kevincianfarini.reddit.MainActivity
import com.kevincianfarini.reddit.RedditApplication
import com.kevincianfarini.reddit.di.AppComponent
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext

abstract class BaseFragment : Fragment() {

    @CallSuper
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        injectDependencies((activity?.application as RedditApplication).appComponent)
    }

    abstract fun injectDependencies(component: AppComponent)
}