package com.kevincianfarini.reddit.feature.postlist.adapter

import android.view.ViewGroup
import com.kevincianfarini.reddit.R
import com.kevincianfarini.reddit.data.model.Post
import com.kevincianfarini.reddit.feature.base.adapter.ViewTypeViewHolder
import com.kevincianfarini.reddit.util.inflate
import kotlinx.android.synthetic.main.list_item_text_post.view.*

class TextPostViewHolder(parent: ViewGroup) : ViewTypeViewHolder<Post>(parent.inflate(R.layout.list_item_text_post)) {

    override fun bind(item: Post) = with(itemView) {
        text_post_title.text = item.title
    }
}