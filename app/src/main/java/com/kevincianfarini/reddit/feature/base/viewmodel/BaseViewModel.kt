package com.kevincianfarini.reddit.feature.base.viewmodel

import androidx.annotation.CallSuper
import androidx.lifecycle.ViewModel
import com.kevincianfarini.reddit.data.network.HttpStatusCodeException
import com.kevincianfarini.reddit.data.network.LoadingStatus
import com.kevincianfarini.reddit.data.network.LoadingType
import kotlinx.coroutines.CancellationException
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.cancel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.Channel.Factory.CONFLATED
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.flow
import kotlin.coroutines.suspendCoroutine

abstract class BaseViewModel(private val scope: CoroutineScope) : ViewModel(), CoroutineScope by scope {

    protected val loadingStatusChannel: Channel<LoadingStatus> = Channel(CONFLATED)
    val loadingStatus: Flow<LoadingStatus> = flow {
        loadingStatusChannel.consumeEach { emit(it) }
    }

    @CallSuper
    override fun onCleared() {
        super.onCleared()
        this.cancel(CancellationException("$this called onCleared"))
    }
}