package com.kevincianfarini.reddit.feature.postlist.adapter

import android.view.ViewGroup
import com.kevincianfarini.reddit.data.model.Post
import com.kevincianfarini.reddit.feature.base.adapter.ViewType
import com.kevincianfarini.reddit.feature.base.adapter.ViewTypeAdapter
import com.kevincianfarini.reddit.feature.base.adapter.ViewTypeViewHolder

class PostAdapter : ViewTypeAdapter<Post>(PostDiffItemCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewTypeViewHolder<Post> = when (viewType) {
        ViewType.PostType.IMAGE.ordinal -> ImagePostViewHolder(parent)
        ViewType.PostType.TEXT.ordinal -> TextPostViewHolder(parent)
        ViewType.PostType.LINK.ordinal -> TextPostViewHolder(parent)
        else -> throw IllegalArgumentException("$viewType is not a valid ViewType")
    }
}