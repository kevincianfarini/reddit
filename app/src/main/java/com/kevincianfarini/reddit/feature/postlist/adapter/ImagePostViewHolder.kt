package com.kevincianfarini.reddit.feature.postlist.adapter

import android.view.ViewGroup
import coil.api.load
import com.kevincianfarini.reddit.R
import com.kevincianfarini.reddit.data.model.Post
import com.kevincianfarini.reddit.feature.base.adapter.ViewTypeViewHolder
import com.kevincianfarini.reddit.util.inflate
import kotlinx.android.synthetic.main.list_item_image_post.view.*

class ImagePostViewHolder(parent: ViewGroup) : ViewTypeViewHolder<Post>(parent.inflate(R.layout.list_item_image_post)) {

    override fun bind(item: Post) {
        itemView.image_post_title.text = item.title
        itemView.post_image.load(item.thumbnail) {
            crossfade(true)
            placeholder(R.drawable.placeholder)
        }
    }
}