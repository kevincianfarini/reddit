package com.kevincianfarini.reddit.feature.postlist.view

import android.os.Bundle
import android.view.*
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.kevincianfarini.reddit.R
import com.kevincianfarini.reddit.data.model.RedditPostSortKey
import com.kevincianfarini.reddit.data.network.LoadingStatus
import com.kevincianfarini.reddit.data.network.RedditAPIManager
import com.kevincianfarini.reddit.di.AppComponent
import com.kevincianfarini.reddit.feature.base.view.BaseFragment
import com.kevincianfarini.reddit.feature.postlist.adapter.PostAdapter
import com.kevincianfarini.reddit.feature.postlist.viewmodel.PostListViewModel
import com.kevincianfarini.reddit.util.inflate
import kotlinx.android.synthetic.main.fragment_post_list.*
import kotlinx.coroutines.flow.collect
import javax.inject.Inject

class PostListFragment : BaseFragment() {

    @Inject lateinit var api: RedditAPIManager
    private lateinit var viewModel: PostListViewModel
    private val postAdapter: PostAdapter = PostAdapter()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return container?.inflate(R.layout.fragment_post_list)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHasOptionsMenu(true)
        initList()
        initRefresh()
        viewModel.loadPosts()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_post_list, menu)
    }

    override fun injectDependencies(component: AppComponent) {
        component.inject(this)
        viewModel = PostListViewModel(api, "androiddev", RedditPostSortKey.BEST)
    }

    override fun onOptionsItemSelected(item: MenuItem) = when (item.itemId) {
        R.id.action_refresh -> {
            viewModel.refreshPosts()
            true
        }
        else -> onSortOptionsItemSelected(item.itemId)
    }

    private fun onSortOptionsItemSelected(itemId: Int): Boolean {
        val sortKey: RedditPostSortKey? = when (itemId) {
            R.id.sort_order_best -> RedditPostSortKey.BEST
            R.id.sort_order_hot -> RedditPostSortKey.HOT
            R.id.sort_order_new -> RedditPostSortKey.NEW
            R.id.sort_order_rising -> RedditPostSortKey.RISING
            R.id.sort_order_controversial_hour -> RedditPostSortKey.CONTROVERSIAL_HOUR
            R.id.sort_order_controversial_day -> RedditPostSortKey.CONTROVERSIAL_DAY
            R.id.sort_order_controversial_week -> RedditPostSortKey.CONTROVERSIAL_WEEK
            R.id.sort_order_controversial_year -> RedditPostSortKey.CONTROVERIAL_YEAR
            R.id.sort_order_controversial_all_time -> RedditPostSortKey.CONTROVERSIAL_ALL_TIME
            R.id.sort_order_top_hour -> RedditPostSortKey.TOP_HOUR
            R.id.sort_order_top_day -> RedditPostSortKey.TOP_DAY
            R.id.sort_order_top_week -> RedditPostSortKey.TOP_WEEK
            R.id.sort_order_top_year -> RedditPostSortKey.TOP_YEAR
            R.id.sort_order_top_all_time -> RedditPostSortKey.TOP_ALL_TIME
            else -> null
        }

        sortKey?.let { viewModel.sortKey = it }

        return sortKey != null
    }

    private fun initRefresh() {
        refresh.setOnRefreshListener {
            viewModel.refreshPosts()
        }

        lifecycleScope.launchWhenResumed {
            viewModel.loadingStatus.collect  { refresh.isRefreshing = getRefreshStatus(it) }
        }
    }

    private fun getRefreshStatus(loadingStatus: LoadingStatus) = when (loadingStatus) {
        is LoadingStatus.Loading -> true
        else -> false
    }

    private fun initList() {
        post_list.apply {
            adapter = postAdapter
            layoutManager = LinearLayoutManager(context)
            setHasFixedSize(true)
            addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                    super.onScrolled(recyclerView, dx, dy)
                    initInfiniteScroll()
                }
            })
        }

        lifecycleScope.launchWhenResumed {
            viewModel.posts.collect { postAdapter.updateList(it) }
        }
    }

    private fun initInfiniteScroll() {
        val layotManager: LinearLayoutManager = post_list.layoutManager as LinearLayoutManager
        if (!refresh.isRefreshing && layotManager.findLastVisibleItemPosition() >= layotManager.itemCount - 1) {
            viewModel.loadPosts()
        }
    }
}