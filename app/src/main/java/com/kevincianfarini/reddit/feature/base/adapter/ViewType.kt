package com.kevincianfarini.reddit.feature.base.adapter

interface ViewType {

    val viewType: PostType

    enum class PostType {
        IMAGE, TEXT, LINK
    }
}