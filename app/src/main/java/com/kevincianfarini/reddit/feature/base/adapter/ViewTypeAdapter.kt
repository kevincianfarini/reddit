package com.kevincianfarini.reddit.feature.base.adapter

import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView

abstract class ViewTypeAdapter<T : ViewType>(
    private val diff: DiffUtil.ItemCallback<T>
) : RecyclerView.Adapter<ViewTypeViewHolder<T>>() {

    private var data: List<T> = emptyList()

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: ViewTypeViewHolder<T>, position: Int) = holder.bind(data[position])

    override fun getItemViewType(position: Int): Int = data[position].viewType.ordinal

    fun updateList(newData: List<T>) {
        val result = DiffUtil.calculateDiff(getCallback(newData))
        data = newData
        result.dispatchUpdatesTo(this)
    }

    private fun getCallback(newData: List<T>): DiffUtil.Callback = object : DiffUtil.Callback() {
        override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return diff.areItemsTheSame(data[oldItemPosition], newData[newItemPosition])
        }

        override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
            return diff.areContentsTheSame(data[oldItemPosition], newData[newItemPosition])
        }

        override fun getOldListSize() = data.size

        override fun getNewListSize() = newData.size
    }
}