package com.kevincianfarini.reddit.feature.postlist.adapter

import androidx.recyclerview.widget.DiffUtil
import com.kevincianfarini.reddit.data.model.Post

class PostDiffItemCallback : DiffUtil.ItemCallback<Post>() {

    override fun areItemsTheSame(oldItem: Post, newItem: Post) = oldItem.url == newItem.url

    override fun areContentsTheSame(oldItem: Post, newItem: Post) = oldItem == newItem
}