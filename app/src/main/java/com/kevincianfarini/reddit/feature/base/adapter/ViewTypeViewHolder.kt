package com.kevincianfarini.reddit.feature.base.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class ViewTypeViewHolder<T : ViewType>(itemView: View) : RecyclerView.ViewHolder(itemView) {
    abstract fun bind(item: T): Unit
}