package com.kevincianfarini.reddit.feature.postlist.viewmodel

import com.kevincianfarini.reddit.data.model.Post
import com.kevincianfarini.reddit.data.model.RedditPostSortKey
import com.kevincianfarini.reddit.data.network.HttpStatusCodeException
import com.kevincianfarini.reddit.data.network.LoadingStatus
import com.kevincianfarini.reddit.data.network.LoadingType
import com.kevincianfarini.reddit.data.network.RedditAPIManager
import com.kevincianfarini.reddit.feature.base.viewmodel.BaseViewModel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.Channel.Factory.CONFLATED
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.channels.consumeEach
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.launch

class PostListViewModel(
    private val api: RedditAPIManager,
    private val subReddit: String,
    sortKey: RedditPostSortKey,
    scope: CoroutineScope = CoroutineScope(Dispatchers.Default + SupervisorJob())
) : BaseViewModel(scope) {

    val posts: Flow<List<Post>> = flow {
        postsChannel.consumeEach {
            loadedPosts = it
            emit(it)
        }
    }
    private var _sortKey: RedditPostSortKey = sortKey
    var sortKey: RedditPostSortKey
        get() = _sortKey
        set(value) {
            _sortKey = value
            refreshPosts()
        }
    private var loadedPosts: List<Post> = emptyList()
    private val postsChannel: Channel<List<Post>> = Channel(CONFLATED)
    private var after: String? = null
    private val loadingType get() = if (after == null) LoadingType.REFRESH else LoadingType.PAGINATED

    fun loadPosts() = launch {
        loadPostsAfter()
    }

    fun refreshPosts() = launch {
        after = null
        postsChannel.offer(emptyList())
        loadPostsAfter()
    }

    private suspend fun loadPostsAfter() {
        try {
            loadingStatusChannel.offer(LoadingStatus.Loading(loadingType))

            val data = api.getSubredditPosts(subReddit, sortKey, after)
            postsChannel.offer(loadedPosts + data.children)
            after = data.after

            loadingStatusChannel.offer(LoadingStatus.Completed)
        } catch (e: HttpStatusCodeException) {
            loadingStatusChannel.offer(LoadingStatus.Error(e))
        }
    }
}